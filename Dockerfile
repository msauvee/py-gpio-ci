FROM resin/raspberry-pi-python:3.6-slim

# Copy the Python application
COPY app app
COPY requirements.txt app
WORKDIR app

RUN apt-get update
RUN pip install --upgrade pip && \
	pip install -r requirements.txt && \
	pip install gunicorn

RUN apt-get autoremove

EXPOSE 8000

CMD ["gunicorn", "-w 4", "pigpioserver:app", "-b 0.0.0.0:8000"]