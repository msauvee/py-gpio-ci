# Overview

This is an example of python project for Raspberry Pi Zero W with GPIO. The main goal is to have CI on this project.

## Requirements

You need a docker store accessible from to push and pull images.
You need docker also.

## Build and deploy

Update ```build.sh``` and ```deploy.sh``` scripts according to the name of the image locally, and in the docker egistry you are using.
Current values are mine:
* name of the image locally : msauvee/py-gpio-ci
* name of the image in the docker registry: registry.sauvee.com:8443/py-gpio-ci

The script ```build.sh``` build the image from sources and use ```Dokerfile``` as docker file.

The script ```deploy.sh``` deploy the built image to the docker registry.

## Run on Raspberry for development

To run on the raspberry, you have to setup you raspberry with docker. I used [HypriotOS](https://blog.hypriot.com/) which is a minimal debian plus docker OS for Raspberry.

First, pull the image:

```
$ docker pull registry.sauvee.com:8443/py-gpio-ci
```

Then run it with access to the device (for GPIO):

```
$ docker run -it --rm --device /dev/gpiomem -v $(pwd)/py-gpio-ci:/root/git/py-gpio-ci registry.sauvee.com:8443/py-gpio-ci:dev  /bin/bash
```

Then get source if not already there:

```
root@d4d30fb33aca:~/git# git clone https://gitlab.com/msauvee/py-gpio-ci
```

## Deploy and run a release

```
$ docker pull registry.sauvee.com:8443/py-gpio-ci
```

Then run it with access to the device (for GPIO):

```
$ docker run --rm --device /dev/gpiomem -p 80:5000 -d registry.sauvee.com:8443/py-gpio-ci
```