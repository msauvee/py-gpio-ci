from flask import Flask, jsonify, request, abort
from gpiozero import LED

app = Flask(__name__);
led = LED(14)

@app.route('/pi/led/14/', methods = ['GET'])
def get_let_14():
    return jsonify( { 'value': led.is_lit } )

@app.route('/pi/led/14/', methods = ['PUT'])
def set_led_14():
    value = request.json.get('value', '')
    if value:
        led.on()
    else:
        led.off()
    return jsonify( { 'value': led.is_lit } )

@app.route('/events/', methods = ['POST'])
def new_event():
    event = request.get_json()
    if 'type' in event and event['type'] == "switch" and 'value' in event and event['value'] == "on":
        print("on")
        led.on()
        return jsonify(event)

    if 'type' in event and event['type'] == "switch" and 'value' in event and event['value'] == "off":
        print("off")
        led.off()
        return jsonify(event)

    abort(400)

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    app.run()