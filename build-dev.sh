#!/bin/bash

docker build \
       -t msauvee/py-gpio-ci:dev \
       -f Dockerfile.dev \
       .

docker tag msauvee/py-gpio-ci:dev registry.sauvee.com:8443/py-gpio-ci:dev

docker push registry.sauvee.com:8443/py-gpio-ci:dev